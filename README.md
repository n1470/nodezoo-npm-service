# nodezoo-npm-service
Service to interact with ``npmjs``.

## Development

### Server with auto-reload
```shell
make app_dev_run
```

In another terminal
```shell
curl -v http://localhost:8080/info/express
curl -v http://localhost:8080/info/react
```

### Setup on K8s

- Local k8s (minikube, ...)

  - Clone [Nodezoo K8s Cluster](https://gitlab.com/n1470/nodezoo-k8s-cluster) repository to a location outside
    of the current one.
  - Follow README.md instructions to set up a local k8s cluster

- Build the app image

```shell
make dev_app_image
make push_dev_app_image
```

#### Workflow
On app or infrastructure changes:
```shell
make dist dev_deploy
kubectl get all -n nodezoo # should list NPM service and deployment 
```

## Production

### Install
Prepare the application docker to be pushed to ECR. The default AWS region is `us-east-1`. To change to a different
region, create a `.makerc` file in the repository directory and add the following entries.

```makefile
# Change AWS region
AWS_REGION := ap-south-1

# Use an aws-cli profile
#AWS_PROFILE := cdk
```

Make a new package, create the ECR repository, create an app image and push the image to ECR.
```shell
make clean package app_repository app_image push_app_image
```

### Deploy
For a production setup, deploy does not deploy the manifests to a K8s cluster. Instead, it submits it to a FLux repository.
Check the `nodezoo-flux` and `nodezoo-k8s-manifests` projects for setting them up correctly before trying a
production setup of this service.
```shell
make dist deploy
```
With the `gitops` setup with GitLab CI, pushing a git tag starting with `aws-rel` will trigger a deployment.

## Useful commands

 * `make app_dev_run`   run dev server with auto-reload
 * `make test`          run all tests
 * `make package`       make the app package for copying to docker
 * `make dist`          emits the synthesized CloudFormation template
 * `make deploy`        deploy this stack to your default AWS account/region

Enjoy!
