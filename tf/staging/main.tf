locals {
  app = "nodezoo"
  environment = "staging"
  region = "ap-south-1"
}

terraform {
  cloud {
    organization = "Sayantam-Learning"
    workspaces {
      name = "npm-service-staging-ap-south1"
    }
  }
}

provider "aws" {
  region = local.region
  profile = var.aws_profile
}

data "terraform_remote_state" "nodezoo_cluster" {
  backend = "remote"

  config = {
    organization = "Sayantam-Learning"
    workspaces = {
      name = "info-system-staging-ap-south1"
    }
  }
}

resource "aws_ecs_task_definition" "npm" {
  execution_role_arn = data.terraform_remote_state.nodezoo_cluster.outputs.ecs_task_execution_role_arn
  container_definitions = jsonencode([{
    name = "nodezoo-npm-service"
    image = "sayantam/nodezoo-npm-service:0.0.1"
    essential = true
    portMappings = [{
      name = "nodezoo-npm-service"
      protocol = "tcp"
      containerPort = 8080
      hostPort = 8080
    }]
    logConfiguration = {
      logDriver = "awslogs",
      options = {
        "awslogs-group" = "nodezoo",
        "awslogs-region" = local.region,
        "awslogs-stream-prefix" = "npm-service"
      }
    }
  }])
  family                = "nodezoo-npm-service"
  requires_compatibilities = ["FARGATE"]
  cpu = 256
  memory = 1024
  network_mode = "awsvpc"
  tags = {
    "Name" = "nodezoo-npm-service"
  }
}

resource "aws_ecs_service" "npm" {
  name = "nodezoo-npm-service"
  cluster = data.terraform_remote_state.nodezoo_cluster.outputs.nodezoo_ecs_cluster_id
  task_definition = aws_ecs_task_definition.npm.arn
  desired_count = 3
  launch_type = "FARGATE"
  scheduling_strategy = "REPLICA"

  network_configuration {
    security_groups = [data.terraform_remote_state.nodezoo_cluster.outputs.nodezoo_sg_ecs_tasks_id]
    subnets = [
      data.terraform_remote_state.nodezoo_cluster.outputs.nodezoo_subnet_private_1_id,
      data.terraform_remote_state.nodezoo_cluster.outputs.nodezoo_subnet_private_2_id
    ]
    assign_public_ip = false
  }

  lifecycle {
    ignore_changes = [task_definition, desired_count]
  }

  service_connect_configuration {
    namespace = data.terraform_remote_state.nodezoo_cluster.outputs.discovery_http_namespace_arn
    service {
      port_name = "nodezoo-npm-service"
      discovery_name = "nodezoo-npm-service"
      client_alias {
        port = 8080
      }
    }
    enabled = true
  }
#  service_registries {
#    registry_arn = data.terraform_remote_state.nodezoo_cluster.outputs.npm_service_registry_arn
#  }
}
