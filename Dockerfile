FROM openjdk:17.0.2-bullseye

ARG PROJECT_PATH

RUN apt-get update && \
    apt-get -y upgrade && \
    curl -fsSL https://deb.nodesource.com/setup_16.x | bash - && \
    apt-get install -y nodejs zip libarchive-tools make && \
    rm -rf /var/lib/apt/lists/*

RUN npm install -g aws-cdk@2.23.0 cdk8s-cli@2.0.48

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm -rf ./aws awscliv2.zip

WORKDIR /builds/$PROJECT_PATH

COPY gradlew.* gradlew settings.gradle.kts /builds/$PROJECT_PATH/
COPY gradle/ /builds/$PROJECT_PATH/gradle/
COPY app/build.gradle.kts app/gradle.properties /builds/$PROJECT_PATH/app/
COPY infrastructure/build.gradle.kts infrastructure/gradle.properties /builds/$PROJECT_PATH/infrastructure/
RUN  ./gradlew :app:build :infrastructure:build -x test --parallel --no-daemon || true

COPY Makefile /builds/$PROJECT_PATH/

CMD ["bash"]
