package tao

import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.server.testing.*
import tao.plugins.configureRouting
import tao.plugins.configureSerialization
import kotlin.test.Test
import kotlin.test.assertEquals

class ApplicationTest {
    @Test
    fun testRoot() = testApplication {
        application {
            configureRouting(DependencyResolver())
            configureSerialization()
        }
        val response = client.get("/")
        assertEquals(HttpStatusCode.OK, response.status)
    }
}