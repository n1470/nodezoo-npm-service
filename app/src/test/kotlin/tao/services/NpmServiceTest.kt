package tao.services

import io.ktor.client.engine.mock.*
import io.ktor.http.*
import io.ktor.utils.io.*
import kotlinx.coroutines.runBlocking
import tao.core.PackageService
import tao.core.PkgInfo
import kotlin.test.Test
import kotlin.test.assertContains

internal class NpmServiceTest {

    @Test
    fun fetchPackageInfo() {
        val mockEngine = MockEngine {
            respond(
                content = ByteReadChannel("""{"name":"nest"}"""),
                status = HttpStatusCode.OK,
                headers = headersOf(HttpHeaders.ContentType, "application/json")
            )
        }
        val service: PackageService = NpmService(engine = mockEngine)
        runBlocking {
            val response: PkgInfo = service.fetchPackageInfo("nest")
            val content = response.provider().decodeToString()
            assertContains(content, Regex("nest"))
        }
    }
}
