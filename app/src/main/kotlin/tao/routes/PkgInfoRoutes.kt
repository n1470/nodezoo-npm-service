package tao.routes

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import tao.core.PackageService
import tao.core.PkgError

fun Route.pkgInfoRouting(packageService: PackageService) {

    route("/info") {
        get("/{pkg}") {
            val pkg = call.parameters["pkg"] ?: return@get call.respondText(
                "Missing pkg",
                status = HttpStatusCode.BadRequest
            )
            try {
                val pkgInfo = packageService.fetchPackageInfo(pkg)
                call.respondBytes(
                    contentType = ContentType("application", "json"),
                    status = pkgInfo.status,
                    provider = pkgInfo.provider
                )
            } catch (error: PkgError) {
                call.respond(status = error.status, message = error.message)
            }
        }
    }
}
