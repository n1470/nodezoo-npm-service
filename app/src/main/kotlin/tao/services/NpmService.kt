package tao.services

import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import tao.core.PackageService
import tao.core.PkgError
import tao.core.PkgInfo

class NpmService(engine: HttpClientEngine?) : PackageService {
    private val httpEngine = engine
    private val npmRegistry = "https://registry.npmjs.org"
    private val block: HttpClientConfig<*>.() -> Unit = {
        install(HttpRequestRetry) {
            retryOnServerErrors(maxRetries = 3)
        }

        install(HttpTimeout) {
            requestTimeoutMillis = 3000
        }
    }

    private fun httpClient(): HttpClient {
        return if (httpEngine != null) {
            HttpClient(httpEngine, block)
        } else {
            HttpClient(block)
        }
    }

    override suspend fun fetchPackageInfo(pkg: String): PkgInfo {
        httpClient().use<HttpClient, String> { client ->
            val npmResponse: HttpResponse = client.get("$npmRegistry/$pkg")
            if (npmResponse.status == HttpStatusCode.OK) {
                return PkgInfo(
                    status = HttpStatusCode.OK,
                    provider = { npmResponse.readBytes() }
                )
            } else {
                throw PkgError(status = npmResponse.status, message = npmResponse.bodyAsText())
            }
        }
    }
}
