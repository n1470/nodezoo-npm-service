package tao

import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.callloging.*
import tao.plugins.configureHTTP
import tao.plugins.configureRouting
import tao.plugins.configureSerialization

fun main() {
    val resolver = DependencyResolver()
    embeddedServer(Netty, port = 8080, host = "0.0.0.0") {
        install(CallLogging)
        configureRouting(resolver)
        configureHTTP()
        configureSerialization()
    }.start(wait = true)
}
