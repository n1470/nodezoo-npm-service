package tao.plugins

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import tao.DependencyResolver
import tao.routes.pkgInfoRouting

fun Application.configureRouting(resolver: DependencyResolver) {

    val packageService = resolver.packageService()

    routing {
        get("/") {
            call.respond(mapOf("_links" to listOf("/info")))
        }

        pkgInfoRouting(packageService)
    }
}
