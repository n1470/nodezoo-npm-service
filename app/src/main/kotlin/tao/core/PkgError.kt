package tao.core

import io.ktor.http.*

class PkgError (
    val status: HttpStatusCode,
    override val message: String
) : Exception(message)
