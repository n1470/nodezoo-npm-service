package tao.core

import io.ktor.http.*

data class PkgInfo(
    val status: HttpStatusCode,
    val provider: suspend () -> ByteArray
)

