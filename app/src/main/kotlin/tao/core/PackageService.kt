package tao.core

interface PackageService {
    suspend fun fetchPackageInfo(pkg: String): PkgInfo
}