package tao

import tao.core.PackageService
import tao.services.NpmService

class DependencyResolver {

    fun packageService() : PackageService {
        return NpmService(engine = null)
    }
}
