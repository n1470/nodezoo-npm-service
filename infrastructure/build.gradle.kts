val junitVersion: String by project
val hamcrestVersion: String by project

plugins {
    application
}

group = "tao"
version = "0.0.1"
application {
    mainClass.set("tao.NpmServiceChart")
}

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/43887532/packages/maven")
}

tasks {
    compileJava {
        sourceCompatibility = JavaVersion.VERSION_17.toString()
        targetCompatibility = JavaVersion.VERSION_17.toString()
    }
    compileTestJava {
        sourceCompatibility = JavaVersion.VERSION_17.toString()
        targetCompatibility = JavaVersion.VERSION_17.toString()
    }
    test {
        useJUnitPlatform()
    }
}

dependencies {
    implementation("tao:nodezoo-service-chart:0.0.3")
    runtimeOnly("ch.qos.logback:logback-classic:1.2.11")

    testImplementation("org.junit.jupiter:junit-jupiter:$junitVersion")
    testImplementation("org.hamcrest:hamcrest:$hamcrestVersion")
    testImplementation("org.mockito:mockito-core:4.5.1")
}
