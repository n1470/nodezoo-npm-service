package tao;

import org.cdk8s.App;
import org.cdk8s.AppProps;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.constructs.Construct;

import java.io.IOException;
import java.nio.file.Files;

public class NpmServiceChart extends PkgServiceChart {

    private static final Logger logger = LoggerFactory.getLogger(PkgServiceChart.class);

    public NpmServiceChart(@NotNull Construct scope,
                           @NotNull String id,
                           @NotNull PkgServiceChartProps pkgServiceChartProps) {
        super(scope, id, pkgServiceChartProps);
    }

    public static void main(String[] args) throws IOException {
        logger.info("Chart options: " + String.join(" ", args));
        final var npmServiceChartProps = PkgServiceChartProps.builder(args).build();
        logger.info("Synthesizing manifests...");
        Files.createDirectories(npmServiceChartProps.getOutputPath());
        final var appProps = AppProps.builder().outdir(npmServiceChartProps.getOutputPath().toString()).build();
        final var app = new App(appProps);
        new NpmServiceChart(app, "npm", npmServiceChartProps);
        app.synth();
    }
}
